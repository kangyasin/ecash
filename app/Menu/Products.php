<?php

namespace App\Menu;

use Sebastienheyd\Boilerplate\Menu\Builder;

class Products
{
    public function make(Builder $menu)
    {
        $item = $menu->add('Products', [
            'permission' => 'backend',
            'active' => 'products',
            'icon' => 'desktop',
            'order' => 100,
        ]);

        $item->add('Category Product', [
            'permission' => 'backend',
            'active' => 'categories.index',
            'route' => 'categories.index',
        ]);

        $item->add('Product Items', [
            'permission' => 'backend',
            'active' => 'items.index',
            'route' => 'items.index',
        ]);
    }
}
