<?php

namespace App\Menu;

use Sebastienheyd\Boilerplate\Menu\Builder;

class PrivacyPolicies
{
    public function make(Builder $menu)
    {
        $menu->add('Privacy Policies', [
            'route' => 'privacy',
            'active' => 'privacy',
            'permission' => 'backend',
            'icon' => 'square',
            'order' => 100,
        ]);
    }
}
