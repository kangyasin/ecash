<?php

namespace App\Menu;

use Sebastienheyd\Boilerplate\Menu\Builder;

class Privacy
{
    public function make(Builder $menu)
    {
        $menu->add('Privacy', [
            'route' => 'privacy.show',
            'active' => 'privacy.show',
            'permission' => 'backend',
            'icon' => 'square',
            'order' => 100,
        ]);
    }
}
