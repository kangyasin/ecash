<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;

class ResponseHelper
{

    /**
     * Formatted Json Response to FrontEnd
     * @param int $code
     * @param $data
     * @param String $message
     * @param $errors
     * @param array $header
     * @return JsonResponse
     */
    public static function json($data, int $code, $message = '', $errors = [], $header = []): JsonResponse
    {
        $response = ['result' => $data, 'message' => $message, 'errors' => $errors];
        return response()->json($response, $code, $header);
    }
}
