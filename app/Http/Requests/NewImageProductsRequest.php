<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class NewImageProductsRequest extends FormRequest
{
    public $validator = null;
    /**
     * for return response message from custom request
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     * @author Yasin
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'numeric|required',
            'name' => 'string|required',
            'description' => 'string|required',
            'link' => 'string|required',
            'main' => 'boolean',
        ];
    }

}
