<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class NewCategoryProductsRequest extends FormRequest
{
    public $validator = null;
    /**
     * for return response message from custom request
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     * @author Yasin
     */
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
        ];

//        return [
//            'name' => 'string|required|unique:mysql.category_products,name,NULL,id,deleted_at,NULL',
//        ];
    }

}
