<?php

namespace App\Http\Controllers;

use App\Models\PrivacyPolicies;
use Illuminate\Http\Request;

class PrivacyPoliciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        //
        $privacy = PrivacyPolicies::first();
        return view('privacy.policy', compact('privacy'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PrivacyPolicies  $privacyPolicies
     * @return \Illuminate\Http\Response
     */
    public function show(PrivacyPolicies $privacyPolicies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PrivacyPolicies  $privacyPolicies
     * @return \Illuminate\Http\Response
     */
    public function edit(PrivacyPolicies $privacyPolicies)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PrivacyPolicies  $privacyPolicies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrivacyPolicies $privacyPolicies)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PrivacyPolicies  $privacyPolicies
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrivacyPolicies $privacyPolicies)
    {
        //
    }

    public function privacy()
    {
        $privacy = PrivacyPolicies::first();
        return view('policy', compact('privacy'));
    }
}
