<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['status_code' => 400, 'message' => 'Bad Request']);
        }

        User::create($validator);
        return ResponseHelper::json(true, 200, 'User created succesfully!');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['status_code' => 400, 'message' => 'Bad Request']);
        }

        $credential = request(['email', 'password']);

        if(!Auth::attempt($credential))
        {
            return response()->json([
                'status_code' => 500,
                'message' => 'Unauthorized'
            ]);
        }

        $header = 'unset';
        if($request->hasHeader('user-platform')){
            $header = $request->header('user-platform');
        }

        $user = User::where('email', $request->email)->first();
        $tokenResult = $user->createToken($header)->plainTextToken;
        return ResponseHelper::json($this->respondWithToken($tokenResult), 200);

//        return response()->json([
//            'user' => $user,
//            'status_code' => 200,
//            'token' => $tokenResult
//        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return ResponseHelper::json(true, 200, 'Token deleted successfully!');
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        $user = User::authTokenFormat($token);
        return response()->json($user);
    }
}
