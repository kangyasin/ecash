<?php

namespace App\Sebastienheyd\Boilerplate\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public $guarded = [];
}
