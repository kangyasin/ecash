<?php

namespace App\Sebastienheyd\Boilerplate\Models;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    public $guarded = [];
}
