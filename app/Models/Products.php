<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'name',
        'description',
        'publish',
    ];

    public function category()
    {
        return $this->belongsTo(CategoryProducts::class,'category_id');
    }

    public function images()
    {
        return $this->hasMany(ImageProducts::class, 'product_id');
    }
}
