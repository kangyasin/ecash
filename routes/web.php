<?php

use App\Http\Controllers\CategoryProductsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ImageProductsController;
use App\Http\Controllers\PrivacyPoliciesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'privacy'], function () {
    Route::resource('/', PrivacyPoliciesController::class, [
        'names' => [
            'privacy' => 'privacy.show',
        ]
    ]);
});

Route::group(['prefix' => 'admin', 'middleware' => ['boilerplateauth', 'ability:admin,backend_access']], function () {

    Route::group(['prefix' => 'privacy-policy'], function () {
        Route::resource('/', PrivacyPoliciesController::class, [
            'names' => [
                'index' => 'privacy',
                'store' => 'privacy.index',
                'update' => 'privacy.update',
                'destroy' => 'privacy.destroy',
                'edit' => 'privacy.edit',
                'create' => 'privacy.create',
            ]
        ]);
    });

    Route::group(['prefix' => 'products'], function () {

        Route::group(['prefix' => 'categories'], function () {
            Route::resource('/', CategoryProductsController::class, [
                'names' => [
                    'index' => 'categories',
                    'store' => 'categories.index',
                    'update' => 'categories.update',
                    'destroy' => 'categories.destroy',
                    'edit' => 'categories.edit',
                    'create' => 'categories.create',
                ]
            ]);
        });

        Route::group(['prefix' => 'items'], function () {
            Route::resource('/', ProductsController::class, [
                'names' => [
                    'index' => 'items',
                    'store' => 'items.index',
                    'update' => 'items.update',
                    'destroy' => 'items.destroy',
                    'edit' => 'items.edit',
                    'create' => 'items.create',
                ]
            ]);
        });

        Route::group(['prefix' => 'images'], function () {
            Route::resource('/', ImageProductsController::class, [
                'names' => [
                    'index' => 'images',
                    'store' => 'images.index',
                    'update' => 'images.update',
                    'destroy' => 'images.destroy',
                    'edit' => 'images.edit',
                    'create' => 'images.create',
                ]
            ]);
        });

    });
});
